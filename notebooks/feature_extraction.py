import os
import pickle
from pathlib import Path
import re

import itertools
import numpy as np
import pandas as pd

from collections import Counter
from functools import reduce

import spacy
import nltk
from nltk.stem import PorterStemmer
from gensim.corpora import Dictionary
from nltk.tokenize import sent_tokenize, word_tokenize, MWETokenizer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from gensim.models.doc2vec import Doc2Vec, TaggedDocument

from spmf import Spmf
from liwc import Liwc

from sklearn.metrics import f1_score, classification_report, confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.decomposition import TruncatedSVD

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
import lightgbm as lgbm
from sklearn.svm import SVC

import optuna
from optuna.integration import LightGBMPruningCallback

from IPython.display import clear_output

def get_agds_test_statistics(
    df_text: pd.DataFrame,
    df_patterns: pd.DataFrame,
    ch_ele_train: dict,
    pdg_train,
    char_cols: list,
    **kwargs,
) -> pd.DataFrame:
    
    """
    Extract features using previously created AGDS on training dataset.
    
    Features created from words:
    - word count
    - unique words
    - avg sentence len
    - std sentence len
    - emotional words count (using Word Lexicon database)
    - unique emotional words 
    
    Features created from training AGDS for every character trait:
    - common words count
    - unique common words 
    - AGDS common words count
    - (N_c_w) / N_all_w) / (N_c / N_all)
    
    - common patterns count
    - unique patterns
    - AGDS common patterns count
    - (N_c_p) / N_all_p) / (N_c / N_all)
    
    + Syntactic features (LIWC + nltk.pos_tag)
    + sentiment
    
    """
    
    gensim_dict = pdg_train.gensim_dict
    df_lex = pd.read_csv(os.path.join(data_dir, 'Emotion_Lexicon.csv'))
    emo_words = df_lex[df_lex.T.iloc[1:].any()].Words.tolist()
    emo_ids = [x for x in pdg_train.gensim_dict.doc2idx(emo_words) if x != -1]
    
    liwc = Liwc(kwargs.get("LIWC_filepath", 'LIWC2007_English100131.dic'))
    
    with open(kwargs.get("LIWC_columns_path", "LIWC_columns.pickle"), 'rb') as f:
            liwc_columns = pickle.load(f)
            f.close()
            
    with open(kwargs.get("tagset_columns_path", "nltk-help-upenn_tagset.pickle"), 'rb') as f:
            tagset_columns = list(pickle.load(f).keys())
            f.close()
    
    analyzer = SentimentIntensityAnalyzer()
    
    ### words
    N_all = pdg_train.n_all_words    

    stat_dict_words = {}       
    
    for _, row in df_text.iterrows():

        pdg_train.add_documents(row.text)
        text = row.text_conc
        bow = dict(gensim_dict.doc2bow(text))        

        author_words = set(bow.keys())
        
        sd = {}
        
        sd['text'] = row.text
        # labels
        sd = sd | row[char_cols].to_dict()

        # syntactic features
        tagset = nltk.FreqDist(tag for (word, tag) in nltk.pos_tag(text))
        
        sd = sd | dict((k, liwc.parse(text).get(k, 0)) for k in liwc_columns)
        sd = sd | dict((k, tagset.get(k, 0)) for k in tagset_columns)
        sd = sd | analyzer.polarity_scores(" ".join(text))
        
        text_unique = np.unique(text)
        sd['word sum'] = len(text)
        sd['unique words'] = len(text_unique)
        sd['len(word) > 6'] = len([x for x in text if len(x) > 6])
        
        sent_lens = [len(x) for x in row.text]
        sd['number of sentences'] = len(row.text)
        sd['avg sentence len'] = np.mean(sent_lens)
        sd['std sentence len'] = np.std(sent_lens)
        
#         author_emo_words = list(author_words.intersection(emo_ids))
        author_emo_words = [x for x in text if x in emo_words]
        sd['emotional words sum'] = len(author_emo_words)
        sd['unique emotional words'] = np.unique(author_emo_words)
        sd['?'] = list(text).count("?")
        sd['!'] = list(text).count("!")
        
        for cc in char_cols:
        
            sd[cc] = row[cc]
        
            for cv in ch_ele_train[cc].keys():

                agds_words = set(ch_ele_train[cc][cv]['word_occ'].keys())
                common_words = agds_words.intersection(author_words)

                sd[cc[1:]+str(cv)+" common words count"] = sum([bow[x] for x in common_words])
                sd[cc[1:]+str(cv)+" unique common words"] = len(common_words)
                sd[cc[1:]+str(cv)+" AGDS common words count"] = sum([ch_ele_train[cc][cv]['word_occ'][x] for x in common_words])
                
#                 sd[cc[1:]+str(cv)+" (N_c_w) / N_all_w) / (N_c / N_all)"] = sum((v / ch_ele_train[cc][cv]['word_occ'].get(k, 0)) / (pdg_train.n_char_words[cc][cv] / pdg_train.n_all_words) \
#                                                                                for k, v in bow.items() if ch_ele_train[cc][cv]['word_occ'].get(k, 0) != 0)
                
#                 sd[cc[1:]+str(cv)+" (N_c_w) / N_all_w) / log(N_all / N_c)"] = sum((v / ch_ele_train[cc][cv]['word_occ'].get(k, 0)) / np.log(pdg_train.n_all_words / pdg_train.n_char_words[cc][cv]) \
#                                                                for k, v in bow.items() if ch_ele_train[cc][cv]['word_occ'].get(k, 0) != 0)
                
#                 sd[cc[1:]+str(cv)+" WOCC dict"] = {x: bow[x] for x in common_words}
                
        stat_dict_words[row.author] = sd
        

    df_word_stats = pd.DataFrame(stat_dict_words).T

    
    ### patterns
    pattern_stat_dict = {}
    
    n_char_patterns = {}
    for cc in char_cols:
        n_char_patterns[cc] = {key: sum(value['pattern_occ'].values()) for key, value in ch_ele_train[cc].items()}
    n_all_patterns = sum(n_char_patterns[cc].values())

    for a, g in df_patterns.groupby('author'):

        sd = {}

        char_traits = df_text.loc[df_text.author == a, char_cols]
        
        bop = g[['pattern', 'sup']].set_index('pattern').to_dict()['sup']        
        author_patterns = set(bop.keys())
        
#         sd["POCC dict"] = bop
        
        sd['pattern count'] = len(author_patterns)
        sd['unique patterns'] = g.sup.sum()
        
        pattern_lens = [len(x) for x in author_patterns]
        sd['avg pattern len'] = np.mean(pattern_lens)
        sd['std pattern len'] = np.std(pattern_lens)
        
        # patterns with emotional words
        sd['emotional patterns sum'] = g[g.pattern.isin([x for x in g.pattern.tolist() if any([a in emo_words for a in x])])].sup.sum()
        sd['unique emotional patterns'] = len([x for x in author_patterns if any([a in emo_words for a in x])])
        
        for cc in char_cols:

            for cv in ch_ele_train[cc].keys():

                agds_patterns = set(ch_ele_train[cc][cv]['pattern_occ'].keys())
                common_patterns = agds_patterns.intersection(author_patterns)

                sd[cc[1:]+str(cv)+" common patterns count"] = g[g.pattern.isin(common_patterns)].sup.sum()
                sd[cc[1:]+str(cv)+" unique common patterns"] = len(common_patterns)
                sd[cc[1:]+str(cv)+" AGDS common patterns count"] = sum([ch_ele_train[cc][cv]['pattern_occ'][x] for x in common_patterns])
                
                
                n_char_patterns = {}
                for cc in char_cols:
                    n_char_patterns[cc] = {key: sum(value['pattern_occ'].values()) for key, value in ch_ele_train[cc].items()}
                n_all_patterns = sum(n_char_patterns[cc].values())
                
        pattern_stat_dict[a] = sd

    df_pattern_stats = pd.DataFrame(pattern_stat_dict).T
    
    df_stats = pd.merge(df_word_stats, df_pattern_stats, left_index=True, right_index=True, how='outer')
    df_stats = df_stats.loc[:,~df_stats.columns.duplicated()]
    
    return df_stats.fillna(0)

def create_D2V_features(df_text_train, df_text, set_index: str='author', **kwargs):
    """
    Function that trains a doc2vec model on train dataset and applies infered vectors to entire dataframe.
    """
    
    documents = [TaggedDocument(row.text_conc, [row.author]) for _, row in df_text_train[['author', 'text_conc']].iterrows()]

    model = Doc2Vec(documents, vector_size=50, window=10, min_count=10, workers=4, epochs=50)

    d2v_features = df_text.set_index(set_index).text_conc.apply(model.infer_vector)
    d2v_features = d2v_features.apply(pd.Series)
    d2v_features = d2v_features.rename(columns=lambda x: 'D2V' + str(x))
    
    return d2v_features

def create_vectorizer_features(df_text_train, df_text, vectorizer=TfidfVectorizer()):
    """
    Function that converts a pandas dataframe with words in list to TF-IDF dataframe.
    """
    
    vectorizer = TfidfVectorizer()

    vectorizer.fit(df_text_train.text_conc.apply(lambda x: " ".join(x)).tolist())
    tfidf_matirx = vectorizer.transform(df_text.text_conc.apply(lambda x: " ".join(x)).tolist())

    df_tfidf = pd.DataFrame(tfidf_matirx.toarray(),
                            index=df_text.author,
                            columns=["VEC"+str(i) for i in range(tfidf_matirx.shape[1])])
    
    return df_tfidf

def get_agds_test_statistics(
    df_text: pd.DataFrame,
    df_patterns: pd.DataFrame,
    ch_ele_train: dict,
    pdg_train,
    char_cols: list,
    **kwargs,
) -> pd.DataFrame:
    
    """
    Extract features using previously created AGDS on training dataset.
    
    Features created from words:
    - word count
    - unique words
    - avg sentence len
    - std sentence len
    - emotional words count (using Word Lexicon database)
    - unique emotional words 
    
    Features created from training AGDS for every character trait:
    - common words count
    - unique common words 
    - AGDS common words count
    - (N_c_w) / N_all_w) / (N_c / N_all)
    
    - common patterns count
    - unique patterns
    - AGDS common patterns count
    - (N_c_p) / N_all_p) / (N_c / N_all)
    
    + Syntactic features (LIWC + nltk.pos_tag)
    + sentiment
    
    """
    
    gensim_dict = pdg_train.gensim_dict
    df_lex = pd.read_csv(os.path.join(data_dir, 'Emotion_Lexicon.csv'))
    emo_words = df_lex[df_lex.T.iloc[1:].any()].Words.tolist()
    emo_ids = [x for x in pdg_train.gensim_dict.doc2idx(emo_words) if x != -1]
    
    liwc = Liwc(kwargs.get("LIWC_filepath", 'LIWC2007_English100131.dic'))
    
    with open(kwargs.get("LIWC_columns_path", "LIWC_columns.pickle"), 'rb') as f:
            liwc_columns = pickle.load(f)
            f.close()
            
    with open(kwargs.get("tagset_columns_path", "nltk-help-upenn_tagset.pickle"), 'rb') as f:
            tagset_columns = list(pickle.load(f).keys())
            f.close()
    
    analyzer = SentimentIntensityAnalyzer()
    
    ### words
    N_all = pdg_train.n_all_words    

    stat_dict_words = {}       
    
    for _, row in df_text.iterrows():

        pdg_train.add_documents(row.text)
        text = row.text_conc
        bow = dict(gensim_dict.doc2bow(text))        

        author_words = set(bow.keys())
        
        sd = {}
        
        sd['text'] = row.text
        # labels
        sd = sd | row[char_cols].to_dict()

        # syntactic features
        tagset = nltk.FreqDist(tag for (word, tag) in nltk.pos_tag(text))
        
        sd = sd | dict((k, liwc.parse(text).get(k, 0)) for k in liwc_columns)
        sd = sd | dict((k, tagset.get(k, 0)) for k in tagset_columns)
        sd = sd | analyzer.polarity_scores(" ".join(text))
        
        text_unique = np.unique(text)
        sd['word sum'] = len(text)
        sd['unique words'] = len(text_unique)
        sd['len(word) > 6'] = len([x for x in text if len(x) > 6])
        
        sent_lens = [len(x) for x in row.text]
        sd['number of sentences'] = len(row.text)
        sd['avg sentence len'] = np.mean(sent_lens)
        sd['std sentence len'] = np.std(sent_lens)
        
#         author_emo_words = list(author_words.intersection(emo_ids))
        author_emo_words = [x for x in text if x in emo_words]
        sd['emotional words sum'] = len(author_emo_words)
        sd['unique emotional words'] = np.unique(author_emo_words)
        sd['?'] = list(text).count("?")
        sd['!'] = list(text).count("!")
        
        for cc in char_cols:
        
            sd[cc] = row[cc]
        
            for cv in ch_ele_train[cc].keys():

                agds_words = set(ch_ele_train[cc][cv]['word_occ'].keys())
                common_words = agds_words.intersection(author_words)

                sd[cc[1:]+str(cv)+" common words count"] = sum([bow[x] for x in common_words])
                sd[cc[1:]+str(cv)+" unique common words"] = len(common_words)
                sd[cc[1:]+str(cv)+" AGDS common words count"] = sum([ch_ele_train[cc][cv]['word_occ'][x] for x in common_words])
                
        stat_dict_words[row.author] = sd
        
    df_word_stats = pd.DataFrame(stat_dict_words).T
    
    ### patterns
    pattern_stat_dict = {}
    
    n_char_patterns = {}
    for cc in char_cols:
        n_char_patterns[cc] = {key: sum(value['pattern_occ'].values()) for key, value in ch_ele_train[cc].items()}
    n_all_patterns = sum(n_char_patterns[cc].values())

    for a, g in df_patterns.groupby('author'):

        sd = {}

        char_traits = df_text.loc[df_text.author == a, char_cols]
        
        bop = g[['pattern', 'sup']].set_index('pattern').to_dict()['sup']        
        author_patterns = set(bop.keys())
        
#         sd["POCC dict"] = bop
        
        sd['pattern count'] = len(author_patterns)
        sd['unique patterns'] = g.sup.sum()
        
        pattern_lens = [len(x) for x in author_patterns]
        sd['avg pattern len'] = np.mean(pattern_lens)
        sd['std pattern len'] = np.std(pattern_lens)
        
        # patterns with emotional words
        sd['emotional patterns sum'] = g[g.pattern.isin([x for x in g.pattern.tolist() if any([a in emo_words for a in x])])].sup.sum()
        sd['unique emotional patterns'] = len([x for x in author_patterns if any([a in emo_words for a in x])])
        
        for cc in char_cols:

            for cv in ch_ele_train[cc].keys():

                agds_patterns = set(ch_ele_train[cc][cv]['pattern_occ'].keys())
                common_patterns = agds_patterns.intersection(author_patterns)

                sd[cc[1:]+str(cv)+" common patterns count"] = g[g.pattern.isin(common_patterns)].sup.sum()
                sd[cc[1:]+str(cv)+" unique common patterns"] = len(common_patterns)
                sd[cc[1:]+str(cv)+" AGDS common patterns count"] = sum([ch_ele_train[cc][cv]['pattern_occ'][x] for x in common_patterns])
                
                
                n_char_patterns = {}
                for cc in char_cols:
                    n_char_patterns[cc] = {key: sum(value['pattern_occ'].values()) for key, value in ch_ele_train[cc].items()}
                n_all_patterns = sum(n_char_patterns[cc].values())
                
        pattern_stat_dict[a] = sd

    df_pattern_stats = pd.DataFrame(pattern_stat_dict).T
    
    df_stats = pd.merge(df_word_stats, df_pattern_stats, left_index=True, right_index=True, how='outer')
    df_stats = df_stats.loc[:,~df_stats.columns.duplicated()]
    
    return df_stats.fillna(0)

def create_D2V_features(df_text_train, df_text, set_index: str='author', **kwargs):
    """
    Function that trains a doc2vec model on train dataset and applies infered vectors to entire dataframe.
    """
    
    documents = [TaggedDocument(row.text_conc, [row.author]) for _, row in df_text_train[['author', 'text_conc']].iterrows()]

    model = Doc2Vec(documents, vector_size=50, window=10, min_count=10, workers=4, epochs=50)

    d2v_features = df_text.set_index(set_index).text_conc.apply(model.infer_vector)
    d2v_features = d2v_features.apply(pd.Series)
    d2v_features = d2v_features.rename(columns=lambda x: 'D2V' + str(x))
    
    return d2v_features

def create_vectorizer_features(df_text_train, df_text, vectorizer=TfidfVectorizer()):
    
    vectorizer = TfidfVectorizer()

    vectorizer.fit(df_text_train.text_conc.apply(lambda x: " ".join(x)).tolist())
    tfidf_matirx = vectorizer.transform(df_text.text_conc.apply(lambda x: " ".join(x)).tolist())

    df_tfidf = pd.DataFrame(tfidf_matirx.toarray(),
                            index=df_text.author,
                            columns=["VEC"+str(i) for i in range(tfidf_matirx.shape[1])])
    
    return df_tfidf
