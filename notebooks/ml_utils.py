import os
import pickle
from pathlib import Path
import re

import itertools
import numpy as np
import pandas as pd

from collections import Counter
from functools import reduce

import seaborn as sns
import matplotlib.pyplot as plt
import time

import spacy
import nltk
from nltk.stem import PorterStemmer
from gensim.corpora import Dictionary
from nltk.tokenize import sent_tokenize, word_tokenize, MWETokenizer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from gensim.models.doc2vec import Doc2Vec, TaggedDocument

from spmf import Spmf
from liwc import Liwc

from sklearn.metrics import f1_score, classification_report, confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.decomposition import TruncatedSVD

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
import lightgbm as lgbm
from sklearn.svm import SVC

import optuna
from optuna.integration import LightGBMPruningCallback

from IPython.display import clear_output

def lgbm_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):

    params = {
        
        "boosting_type": trial.suggest_categorical("boosting_type", ["gbdt", "dart"]),
        'lambda_l1': trial.suggest_float('lambda_l1', 1e-8, 10.0),
        'lambda_l2': trial.suggest_float('lambda_l2', 1e-8, 10.0),
        'num_leaves': trial.suggest_int('num_leaves', 2, 256),
        'feature_fraction': trial.suggest_float('feature_fraction', 0.4, 1.0),
        'bagging_fraction': trial.suggest_float('bagging_fraction', 0.4, 1.0),
        'bagging_freq': trial.suggest_int('bagging_freq', 1, 7),
        'min_child_samples': trial.suggest_int('min_child_samples', 5, 100),
    }


    model = lgbm.LGBMClassifier(objective="binary", **params, random_state=52, seed=52)
    model.fit(
        X_train,
        y_train,
#         eval_set=[(X_val, y_val)],
        eval_metric="binary_logloss",
#         early_stopping_rounds=100,
#         callbacks=[
#             LightGBMPruningCallback(trial, "binary_logloss")
#         ],  # Add a pruning callback
    )
    
    
    y_pred = model.predict(X_test)
    return f1_score(y_test, y_pred, average='weighted')

def rf_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):
        params = {
            'n_estimators': trial.suggest_int('n_estimators', 50, 1000),
            'max_depth': trial.suggest_int('max_depth', 4, 50),
            'min_samples_split': trial.suggest_int('min_samples_split', 1, 150),
            'min_samples_leaf': trial.suggest_int('min_samples_leaf', 1, 60),
        }

        smoth_n_neighbors = trial.suggest_int('smoth_n_neighbors', 5, 10)
        #sampler = SMOTE(random_state=42, k_neighbors=smoth_n_neighbors)

        model = RandomForestClassifier(random_state=52, **params)
        model.fit(X_train, y_train)
        
        y_pred = model.predict(X_test)
        return f1_score(y_test, y_pred, average='weighted')

def svm_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):
        params = {
            
            'kernel': trial.suggest_categorical('kernel', ['linear', 'poly', 'rbf', 'sigmoid']),
            'degree': trial.suggest_int('degree', 1, 8),
            'C': trial.suggest_float('C', 1e-2, 1e2),
            'gamma': trial.suggest_float('gamma', 1e-3, 1e2),
                                                           
        }
        
        model = SVC(probability=False, **params, random_state=52)
        model.fit(X_train, y_train)
        
        y_pred = model.predict(X_test)
        return f1_score(y_test, y_pred, average='weighted')
    
def logreg_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):

    penalty = trial.suggest_categorical("penalty", ["l1", "l2", "elasticnet", "none"])
    if penalty == "elasticnet":
        l1_ratio = trial.suggest_float("l1_ratio", 0.0, 1.0)
    else:
        l1_ratio = None
    C = trial.suggest_float("C", 1e-5, 1e5, log=True)
    
    model = LogisticRegression(penalty=penalty, l1_ratio=l1_ratio, C=C, solver="saga",
                               random_state=52)
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    return f1_score(y_test, y_pred, average='weighted')

def nb_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):

    var_smoothing = trial.suggest_float("var_smoothing", 1e-15, 1e-1, log=True)
    
    # Train and evaluate a Naive Bayes classifier with the chosen hyperparameters
    model = GaussianNB(var_smoothing=var_smoothing)
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    return f1_score(y_test, y_pred, average='weighted')

def lda_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):
    
    solver = trial.suggest_categorical("solver", ["svd", "lsqr", "eigen"])
    shrinkage = trial.suggest_float("shrinkage", 0.0, 1.0)
    
    if solver == "svd":
        shrinkage=None
        
    model = LinearDiscriminantAnalysis(solver=solver, shrinkage=shrinkage)
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    return f1_score(y_test, y_pred, average='weighted')

def kmeans_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):

    init = trial.suggest_categorical('init', ['k-means++', 'random'])
    max_iter = trial.suggest_int('max_iter', 100, 1000, step=100)
    tol = trial.suggest_float('tol', 1e-5, 1e-1)
    algorithm = trial.suggest_categorical('algorithm', ['lloyd', 'elkan', 'auto', 'full'])
    
    # Train KMeans model
    model = KMeans(n_clusters=2, init=init, max_iter=max_iter,
                   tol=tol, algorithm=algorithm, random_state=52)
    model.fit(X_train)
    
    y_pred = model.predict(X_test)
    return f1_score(y_test, y_pred, average='weighted')
    
def knn_objective(trial: optuna.trial.Trial, X_train, y_trian, X_test, y_test):
    
    weights = trial.suggest_categorical("weights", ["uniform", "distance"])
    p = trial.suggest_int("p", 1, 2)
    algorithm = trial.suggest_categorical("algorithm", ['auto', 'ball_tree', 'kd_tree', 'brute'])
    
    model = KNeighborsClassifier(n_neighbors=2, weights=weights, p=p, algorithm=algorithm)
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    return f1_score(y_test, y_pred, average='weighted')
    
def print_results_scores(results_dir) -> pd.DataFrame:
    """
    Function that reads all pickle files from given directory with results files,
    and returns a pandas dataframes with all concatenated scores.
    """
    
    df_list = []

    for r in os.listdir(results_dir):

        with open(os.path.join(results_dir, r), 'rb') as f:
            results = pickle.load(f)
            f.close()

        for df_name, rd in results.items():
            n = r.split("_")[1].split(".")[0] +"_"+ "".join(re.findall(r'\d+', df_name)) + "_".join(df_name.split("_")[6:])
            df = pd.DataFrame(rd).T.rename({"f1score": n}, axis='columns')[n]
        
            df_list.append(df)

    df_results = pd.concat(df_list, axis=1).T
    
    return df_results

def show_values(axs, orient="v", space=.01):
    def _single(ax):
        if orient == "v":
            for p in ax.patches:
                _x = p.get_x() + p.get_width() / 2
                _y = p.get_y() + p.get_height() + (p.get_height()*0.01)
                value = '{:.1f}'.format(p.get_height())
                ax.text(_x, _y, value, ha="center") 
        elif orient == "h":
            for p in ax.patches:
                _x = p.get_x() + p.get_width() + float(space)
                _y = p.get_y() + p.get_height() - (p.get_height()*0.5)
                value = '{:.1f}'.format(p.get_width())
                ax.text(_x, _y, value, ha="left")

    if isinstance(axs, np.ndarray):
        for idx, ax in np.ndenumerate(axs):
            _single(ax)
    else:
        _single(axs)

def visualize_feature_importances(FI, **kwargs):
    """
    Visualize feature FI using Seaborn.

    Args:
        FI (pd.Series): Pandas Series object containing feature FI, with feature names as index.
    """
    
    sns.set_style(kwargs['style'] if 'style' in kwargs else 'darkgrid')
    sns.set_palette(kwargs['palette'] if 'palette' in kwargs else "colorblind")
    color = kwargs.get('color', '#898AFF')

    n = kwargs.get('n', len(FI))
    title = kwargs.get('title', 'Feature importance')

    FI = pd.DataFrame(FI.sort_values(ascending=False)[:n],
                               columns=['Value']).reset_index()
    
#     plt.figure(figsize=(5, n//4))
    splot = sns.barplot(x = "Value", y = "index", data=FI, color=color)
    
    for i in splot.containers:
        splot.bar_label(i, label_type='edge', color='#FF006C', size=9)

#     for p in splot.patches:
#         splot.annotate(f'{int(p.get_height())}',
#                        (p.get_x()-65, p.get_height(), ),
#                        color='white', )
    plt.xlabel('Importance')
    #plt.ylabel('Feature name')
    plt.ylabel('')
    plt.title(title)
