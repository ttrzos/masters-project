import os
import pickle
from pathlib import Path
import re

import itertools
import numpy as np
import pandas as pd

from collections import Counter
from functools import reduce

import spacy
import nltk
from nltk.stem import PorterStemmer
from gensim.corpora import Dictionary
from nltk.tokenize import sent_tokenize, word_tokenize, MWETokenizer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from gensim.models.doc2vec import Doc2Vec, TaggedDocument

from spmf import Spmf
from liwc import Liwc

from sklearn.metrics import f1_score, classification_report, confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.decomposition import TruncatedSVD

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
import lightgbm as lgbm
from sklearn.svm import SVC

import optuna
from optuna.integration import LightGBMPruningCallback

from IPython.display import clear_output

class PersonalityDataGenerator:

    
    def __init__(self, df: pd.DataFrame, df_name: str,
                 preprocess: bool=True, keep_stop_words: bool=False,
                 lemma: bool=False, **kwargs):
        """
        df: pandas dataframe with columns:
            - author: int,str; author of the text
            - text: str; text written by the author
            
        kwargs:
            author_col: str, name of column with author ids
            text_col: str, name of column with texts
            char_cols: list[str], names of columns with character traits
            word_preprocessing: str: type of preprocessing applied to every word: 'raw', 'stemm', 'lemm' available. 
                                Default: 'raw'
            
        """      
        
        self.keep_stop_words = keep_stop_words
        self.lemma = lemma
        self.preprocess = preprocess
        
        self.author_col = kwargs.get('author_col', 'author')
        self.text_col = kwargs.get('text_col', 'text')
        self.char_cols = kwargs.get('char_cols', ['cEXT', 'cNEU', 'cAGR', 'cCON', 'cOPN'])
        self.word_preprocessing = kwargs.get("word_preprocessing", 'raw')
        self.extra_stopwords = kwargs.get("extra_stopwords", {'urllink', 'nbsp'})
        self.df_name = df_name
        
        self.df = df[[self.author_col, self.text_col] + self.char_cols]
        self.df[self.author_col] = self.df[self.author_col].astype(str)
        
        if self.preprocess:
            self.df.text = self.df.text.apply(lambda x: self.preprocess_text(x,
                                                                             keep_stop_words=keep_stop_words,                                                                                             
                                                                             lemma=self.lemma,
                                                                             extra_stopwords=self.extra_stopwords)) 
        
        self.gensim_dict = Dictionary([x for y in self.df[self.text_col].tolist() for x in y])
        self.n_all_words = self.df[self.text_col].apply(lambda x: len(np.concatenate(x))).sum()
        
        self.n_char_words = {}
        for cc in self.char_cols:
            self.n_char_words[cc] = {value: g[self.text_col].apply(lambda x: len(np.concatenate(x))).sum() for value, g in self.df[self.char_cols+[self.text_col]].groupby(cc)}
    
    @staticmethod
    def cols_as_list(df):
        return df.apply(lambda x: x.values, axis=1)
                                      
    @staticmethod
    def preprocess_text(text: str, keep_stop_words: bool=True, lemma: bool=False, **kwargs) -> pd.DataFrame:
        """
        Function used to preprocess given text into word arrays that represent sentences.
        
        """
        
        with open(kwargs.get('pairs_path', 'pairs.pickle'), 'rb') as f:
            pairs = pickle.load(f)
            f.close()
        
        extra_stopwords = kwargs.get("extra_stopwords", {'urllink', 'nbsp'})
        tokenizer = MWETokenizer(pairs, separator="'")
        nlp = spacy.load('en_core_web_sm')
        nlp.Defaults.stop_words |= extra_stopwords
        
        text = text.lower()
        # Replace newline and tab characters with spaces
        text = re.sub(r'\n|\t', ' ', text)
        # Remove non-alphanumeric characters except apostrophe and whitespace
        text = re.sub(r'[^\w\s\.?!,]', ' ', text)

        # Remove excess whitespace
        text = re.sub(r'\s+', ' ', text).strip()
        text = re.sub(r'[.]+', r'.', text)
        text = re.sub(r'[!]+', r'!', text)
        text = re.sub(r'[?]+', r'?', text)

        sentences = sent_tokenize(text)

        if keep_stop_words and lemma:
            sentences = [tokenizer.tokenize([str(token.lemma_) for token in nlp(sent)]) \
                            for sent in sent_tokenize(text)]

        elif keep_stop_words and not lemma:
            sentences = [tokenizer.tokenize([str(token) for token in nlp(sent)]) \
                            for sent in sent_tokenize(text)]

        elif not keep_stop_words and lemma:

            sentences = [tokenizer.tokenize([str(token.lemma_) for token in nlp(sent) if not token.is_stop]) \
                             for sent in sent_tokenize(text)]

        elif not keep_stop_words and not lemma:
            sentences = [tokenizer.tokenize([str(token) for token in nlp(sent) if not token.is_stop]) \
                             for sent in sent_tokenize(text)]
            
        return sentences      
    
    @staticmethod
    def pattern_file_to_csv(path: str) -> pd.DataFrame:
        df = pd.read_csv(path, sep='#SUP:', names=['pattern', 'sup'])
        df.pattern = df.pattern.apply(lambda x: tuple(eval('['+x.replace(" ", "").replace("-1", ",")+']')))
        df.sup = df.sup.astype('int')
        return df
    
    @staticmethod
    def create_nosep_file(words, filename: str, **kwargs):
        """
        words: list, np.array; 
        filename: str; file to create
        """
        
        dct = kwargs.get("dct", Dictionary(words))
        
        words = [x for x in words if x != []]
        idxs = [dct.doc2idx(s) for s in words]

        with open(filename, 'a') as f:
            nosep_string = re.sub(' +', ' ', str(idxs)[2:-2]).replace(" [", "").replace("],", " -2\n").replace(", ", " -1 ") + " -2"
            f.write(nosep_string)

        return nosep_string, dct
    
    @staticmethod
    def mine_patterns(filename, output_filename, dct: dict,
                  minlen, maxlen, mingap, maxgap, minsup,
                  algorithm: str = "NOSEP", **kwargs) -> str:
    
        spmf = Spmf(algorithm, input_filename=filename, output_filename=output_filename,
                    arguments=[minlen, maxlen, mingap, maxgap, minsup])
        spmf.run()

        df_patterns = spmf.to_pandas_dataframe()

        return df_patterns
    
    def extract_patterns(self, sentences: list, author: str,
                         minlen: int, maxlen: int, mingap: int, maxgap: int,
                         minsup: int, nosep_dir: str, pattern_dir: str,
                         override: bool = True, **kwargs):
        
        algorithm = kwargs.get('algorithms', "NOSEP")
        n_largest = kwargs.get('n_largest', [])
        
        
        dir_name = f"minlen{minlen}_maxlen{maxlen}_minlen{minlen}_maxgap{maxgap}_minsup{minsup}{'_no-stop-words' if not self.keep_stop_words else ''}{'_lemma' if self.lemma else ''}"
        
        if dir_name not in os.listdir(nosep_dir):
            nosep_dir = os.path.join(nosep_dir, dir_name)
            os.mkdir(nosep_dir)
        else:
            nosep_dir = os.path.join(nosep_dir, dir_name)
        
        if dir_name not in os.listdir(pattern_dir):
            pattern_dir = os.path.join(pattern_dir, dir_name)
            os.mkdir(pattern_dir)
        else:
            pattern_dir = os.path.join(pattern_dir, dir_name)      
            
        file_str = str(author)
        filename = os.path.join(nosep_dir, file_str + ".txt") 
        output_filename = filename.split(".txt")[0] + "_output.txt"
        
        if file_str + ".csv" in os.listdir(pattern_dir):
            print(f"{file_str}.csv file already found!\n")
            df_patterns = pd.read_csv(os.path.join(pattern_dir, file_str + ".csv"), index_col=0)
            df_patterns.author = df_patterns.author.astype(str)
            df_patterns.pattern = df_patterns.pattern.apply(eval)
            
            if n_largest:
                    df_patterns = df_patterns.nlargest(n_largest, "sup")
            
            return df_patterns
        
        elif file_str + "_output.txt" in os.listdir(nosep_dir):
            print(f"{file_str}_output.txt file already found!\nConverting to csv!\n")
            
            df_patterns = self.pattern_file_to_csv(output_filename)
            if df_patterns.empty:
                return pd.DataFrame(columns=['pattern', 'sup', 'author'])
            else:
                df_patterns['pattern'] = df_patterns.pattern.apply(lambda x: [int(w) for w in x])
                df_patterns.pattern = df_patterns.pattern.apply(tuple)
                df_patterns = df_patterns[df_patterns.pattern.apply(lambda x: True if len(x) > 2 else False)]
                df_patterns['author'] = author
                df_patterns.author = df_patterns.author.astype(str)
                df_patterns['sup'] = df_patterns.sup.astype('int')

                df_patterns.to_csv(os.path.join(pattern_dir, file_str + ".csv"))
                
                if n_largest:
                    df_patterns = df_patterns.nlargest(n_largest, "sup")

                return df_patterns
        
        elif file_str + ".txt" in os.listdir(nosep_dir) and override:
            print("Overwritting!")
            os.remove(filename)
            
        elif file_str + ".txt" in os.listdir(nosep_dir) and not override:
            print(f"File: \n{filename}\nalready exists and override is off!")
            return pd.DataFrame(columns=['pattern','sup','author','word_pattern']).astype({"sup": 'int'})
        
        else:
            
            try: 
                
                filename = os.path.join(nosep_dir, file_str + ".txt") 
                output_filename = filename.split(".txt")[0] + "_output.txt"


                _, _ = self.create_nosep_file(sentences, filename=filename, dct=self.gensim_dict)
                print(f'Created {algorithm} file: {filename}')        

                print("Mining patterns...")
                df_patterns = self.mine_patterns(filename=filename, dct=self.gensim_dict,
                                                 output_filename=output_filename,
                                                 minlen=minlen, maxlen=maxlen, mingap=mingap, 
                                                 maxgap=maxgap, minsup=minsup, algorithm=algorithm)

                df_patterns['pattern'] = df_patterns.pattern.apply(lambda x: (int(w) for w in x))
                df_patterns.pattern = df_patterns.pattern.apply(tuple)
                df_patterns = df_patterns[df_patterns.pattern.apply(lambda x: True if len(x) > 2 else False)]
                df_patterns['author'] = author
                df_patterns.to_csv(os.path.join(pattern_dir, file_str + ".csv"))

                if n_largest:
                    df_patterns = df_patterns.nlargest(n_largest, "sup")

                return df_patterns
        
            except AttributeError:
                return pd.DataFrame(columns=['pattern', 'sup', 'author']).astype({"sup": 'int'})
        
    
    def get_word_pattern(self, df_patterns: pd.DataFrame) -> pd.Series:
        self.id2token = {v: k for k, v in self.gensim_dict.token2id.items()} 
        return df_patterns.pattern.apply(lambda x: [self.id2token[int(w)] for w in x])
    
    def extract_patterns_from_df(self, minlen: int, maxlen: int,
                                 mingap: int, maxgap: int, minsup: int,
                                 nosep_dir: str, pattern_dir: str,
                                 combined_dir: str, override: bool = True, 
                                 **kwargs):
        
        print(f"\n---------- Extracting patterns for {self.df[self.author_col].nunique()} authors! ----------")
        
        n_largest = kwargs.get("n_largest", [])
        override = kwargs.get("override", True)
    
        df = pd.concat(self.df.apply(lambda row: self.extract_patterns(row[self.text_col],
                                                                      row[self.author_col],
                                                                      minlen=minlen, maxlen=maxlen,
                                                                      mingap=mingap, maxgap=maxgap, minsup=minsup,
                                                                      nosep_dir=nosep_dir, pattern_dir=pattern_dir,
                                                                      n_largest=n_largest, override=override),
                                    axis=1).tolist()).reset_index(drop=True)
        
        df.author = df.author.astype(str)
        
        print("SAVING COMBINED!")
        df_x = df.copy()
        df_x.pattern = df_x.pattern.apply(lambda x: tuple([self.gensim_dict[w] for w in x]))
        df_x.to_csv(os.path.join(combined_dir,
                                 f"{self.df_name}_patterns_combined_minlen{minlen}_maxlen{maxlen}_minlen{minlen}_maxgap{maxgap}_minsup{minsup}{'_no-stop-words' if not self.keep_stop_words else ''}{'_lemma' if self.lemma else ''}.csv"))
        
        return df[['pattern', 'sup', 'author']]
    
    def save_gensim_dict(self, path):
        self.gensim_dict.save(path)
                                      
    def get_df(self):
        return self.df.copy()
    
    def get_dictionary(self):
        return self.gensim_dict
    
    def get_prep_ids(self, df: pd.DataFrame, col_name: str):
        return df[col_name].apply(lambda x: [self.gensim_dict.token2id[w] for y in x for w in y])
    
    def add_documents(self, docs):
        self.gensim_dict.add_documents(docs)
    
    def create_word_element(self, df_patterns):

        print("""

        Word element architecture:

        {
            word_1: {
                        'occ': word occurence, 
                        'patterns': [patterns that contain this word],
                        'author': [authors using this word],
                        'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                        .
                        .
                        .
                        'char_colx': [char_colm_1, char_colm_2, ..., char_colm_n]
                    },

            .
            .
            .

            word_n: { 
                        'occ': word occurence, 
                        'pattern': [patterns that contains this word],
                        'author': [authors using this word],
                        'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                        .
                        .
                        .
                        'char_colx': [char_colm_1, char_colm_2, ..., char_colm_n]
                    },
        }

        """)
        
        if 'prep_ids' not in self.df.columns:
            self.df['prep_ids'] = self.get_prep_ids(self.df, col_name=self.text_col)

        return {word: {
                
                 # number of word occurence
                 'word_occ': self.gensim_dict.cfs[word],
                
                # patterns that contain this word
                 'patterns': df_patterns[['pattern']][df_patterns.pattern.str.contains(word, regex=False)].pattern.tolist()} | \
                
                # authors and characters using this word
                 dict((k, np.unique(v).tolist()) for k, v in self.df[['author', 'prep_ids']+char_cols][self.df.prep_ids.str.contains(word, regex=False)][['author']+self.char_cols].to_dict('list').items())

                for word in self.gensim_dict.token2id.values()
                }
        
    def create_pattern_element(self, df_patterns):
                
        print("""
        Pattern element architecture:

        { 
            pattern_1: {
                        'pattern_occ': pattern occ, 
                        'author': [author_1, author_2, ..., author_n],
                        'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                        .
                        .
                        .
                        'char_colm': [char_colm_1, char_colm_2, ..., char_colm_n]
            },
            .
            .
            .
            pattern_n: {
                        'pattern_occ': pattern occ, 
                        'author': [author_1, author_2, ..., author_n],
                        'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                        .
                        .
                        .
                        'char_colm': [char_colm_1, char_colm_2, ..., char_colm_n]
            }
        }
        """)        
        
        if not all(elem in list(df_patterns.columns) for elem in self.char_cols):
            df_patterns = df_patterns.merge(self.df[self.char_cols+['author']])        

        return {pattern: 
         
                     g.agg({'sup': 'sum', 'author': 'unique'}).rename({'sup':'pattern_occ'}).to_dict() | \
                     dict((k, np.unique(v)) for k, v in g[self.char_cols].to_dict('list').items())

                     for pattern, g in df_patterns.groupby('pattern')
                }
    
    def create_author_element(self, df_patterns: pd.DataFrame):
        
        print("""
        Author element architecture:
        
        {
            author_1: {
                        word_id1: {word_id1: word_id1_occ, ... word_id_n: word_id_n_occ},
                        pattern_1: {pattern_1_occ, ... pattern_n: pattern_n_occ},
                        char_col_1: value,
                        .
                        .
                        .
                        char_col_n: value,
                        
            },
            .
            .
            .

            author_n: {
                        word_id1: {word_id1: word_id1_occ, ... word_id_n: word_id_n_occ},
                        pattern_1: {pattern_1_occ, ... pattern_n: pattern_n_occ},
                        char_col_1: value,
                        .
                        .
                        .
                        char_col_n: value,
                        
            },
        }        
        """)
        
        if 'prep_ids' not in self.df.columns:
            self.df['prep_ids'] = self.get_prep_ids(self.df, col_name=self.text_col)
            
        if not self.pattern_occ:
            self.pattern_occ = df_patterns[['pattern', 'sup']].groupby('pattern').agg({"sup": np.sum}).to_dict()['sup']
            
        return {author: {
    
                    #words of an author with its occurences
                    'word_occ': dict(self.gensim_dict.doc2bow([self.gensim_dict[w] for w in np.concatenate(gt.prep_ids.values)])),

                    # patterns of an author with its occurences
                    'pattern_occ': gp.set_index('pattern').sup.to_dict() \

                    # character cols
                    | gt[self.char_cols].to_dict('list'),

                    } for (author, gt), (_, gp) in zip(self.df[['author', 'text', 'prep_ids']+char_cols].groupby('author'),
                                                       df_patterns[['author', 'pattern', 'sup']].groupby('author'))
            }
    
    def create_character_element(self, df_patterns):
        print("""
        
        Character element architecture:
        
        {
            character_1: { 
                            'word_occ': {word_1: word_1_occ, ... word_n: word_n_occ},
                            'pattern_occ': {pattern_1: pattern_1_occ, ... pattern_n: pattern_n_occ},
                            'author': [author_1, ..., author_n]
                         },                
            .
            .
            .

            character_n: { 
                            'word_occ': {word_1: word_1_occ, ... word_n: word_n_occ},
                            'pattern_occ': {pattern_1: pattern_1_occ, ... pattern_n: pattern_n_occ},
                            'author': [author_1, ..., author_n]
                         }, 

        }
        """)
        
        if 'prep_ids' not in self.df.columns:
            self.df['prep_ids'] = self.get_prep_ids(self.df, col_name=self.text_col)
        
        if not all(elem in list(df_patterns.columns) for elem in self.char_cols):
            df_patterns = df_patterns.merge(self.df[self.char_cols+['author']])
        
        return {cc: {ch_v: {

                    # word occurences that this trait of character uses
                    'word_occ': dict(self.gensim_dict.doc2bow([self.gensim_dict[w] for w in np.concatenate(gt.prep_ids.values)])),

                    # pattern occurences that this trait of character uses
                    'pattern_occ': gp[['pattern', 'sup']].groupby('pattern').agg({"sup": 'sum'}).to_dict()['sup'],
                    # old gp.set_index('pattern').sup.to_dict(),

                     # authors with this character trait
                     'author': gt[self.author_col].unique().tolist(),


                 } for (ch_v, gt), (_, gp) in zip(self.df.groupby(cc),
                                                  df_patterns[self.char_cols+['pattern', 'sup']].groupby(cc))
                } for cc in self.char_cols}
    
    
    def get_AGSD_structure(self, df_patterns: pd.DataFrame) -> dict:
        
        print("""
        AGSD architecture structure:
        

        {
                word_1: {
                            'occ': word occurence, 
                            'patterns': [patterns that contain this word],
                            'author': [authors using this word],
                            'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                            .
                            .
                            .
                            'char_colx': [char_colm_1, char_colm_2, ..., char_colm_n]
                        },

                .
                .
                .

                word_n: { 
                            'occ': word occurence, 
                            'pattern': [patterns that contains this word],
                            'author': [authors using this word],
                            'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                            .
                            .
                            .
                            'char_colx': [char_colm_1, char_colm_2, ..., char_colm_n]
                        },

                pattern_1: {
                            'pattern_occ': pattern occ, 
                            'author': [author_1, author_2, ..., author_n],
                            'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                            .
                            .
                            .
                            'char_colm': [char_colm_1, char_colm_2, ..., char_colm_n]
                },
                .
                .
                .
                pattern_n: {
                            'pattern_occ': pattern occ, 
                            'author': [author_1, author_2, ..., author_n],
                            'char_col1': [char_col1_1, char_col1_2, ..., char_col1_n]
                            .
                            .
                            .
                            'char_colm': [char_colm_1, char_colm_2, ..., char_colm_n]
                },

                author_1: {
                            word_id1: {word_id1: word_id1_occ, ... word_id_n: word_id_n_occ},
                            pattern_1: {pattern_1_occ, ... pattern_n: pattern_n_occ},
                            char_col_1: value,
                            .
                            .
                            .
                            char_col_n: value,

                },
                .
                .
                .

                author_n: {
                            word_id1: {word_id1: word_id1_occ, ... word_id_n: word_id_n_occ},
                            pattern_1: {pattern_1_occ, ... pattern_n: pattern_n_occ},
                            char_col_1: value,
                            .
                            .
                            .
                            char_col_n: value,

                },

                character_1: { 
                                'word_occ': {word_1: word_1_occ, ... word_n: word_n_occ},
                                'pattern_occ': {pattern_1: pattern_1_occ, ... pattern_n: pattern_n_occ},
                                'author': [author_1, ..., author_n]
                             },                
                .
                .
                .

                character_n: { 
                                'word_occ': {word_1: word_1_occ, ... word_n: word_n_occ},
                                'pattern_occ': {pattern_1: pattern_1_occ, ... pattern_n: pattern_n_occ},
                                'author': [author_1, ..., author_n]
                             }, 
        }
        """)
        
        return self.create_word_element(df_patterns) | self.create_pattern_element(df_patterns) | \
               self.create_author_element(df_patterns) | self.create_character_element(df_patterns)
    
    ### PREDICT
    
    def create_new_element(self, new_text: str, author: str,
                           nosep_dir: str, pattern_dir: str,
                           word_occ_mode: str='raw', **kwargs):
        
        emo_words = kwargs.get('emo_words', [])
        
        
        if self.preprocess:        
            new_prep = self.preprocess_text(new_text, 
                                            keep_stop_words=self.keep_stop_words,
                                            lemma=self.lemma)
        else:
            new_prep = new_text
        
        self.add_documents(new_prep)
        new_prep_ids = [self.gensim_dict.token2id[w] for y in new_prep for w in y]
        new_patterns = pdg.extract_patterns(new_prep, author, minlen=minlen, maxlen=maxlen,
                                                   mingap=mingap, maxgap=maxgap, minsup=minsup,
                                                   nosep_dir=nosep_dir, pattern_dir=pattern_dir)
        
        if word_occ_mode == 'raw':
            new_element = {
                'text_prep': new_prep,
                'prep_ids': new_prep_ids,
                'word_occ': dict(pdg.gensim_dict.doc2bow([pdg.gensim_dict[w] for w in new_prep_ids])),
                'pattern_occ': new_patterns[['sup', 'pattern']].set_index('pattern').to_dict()['sup'],
            }
        elif word_occ_mode == 'middle':
            
            stem = PorterStemmer()
            
            new_element = {
                'text_prep': new_prep,
                'prep_ids': new_prep_ids,
                'word_occ': dict(pdg.gensim_dict.doc2bow([x for x in np.concatenate(new_prep) if stem.stem(x) in emo_words])),
                'pattern_occ': new_patterns[['sup', 'pattern']].set_index('pattern').to_dict()['sup'],
            }
            
        elif word_occ_mode == 'bottom':
            
            stem = PorterStemmer()
            
            new_element = {
                'text_prep': new_prep,
                'prep_ids': new_prep_ids,
                'word_occ': dict(pdg.gensim_dict.doc2bow(np.concatenate([s for s in new_prep if any(x in emo_words for x in [stem.stem(x) for x in s])]))),
                'pattern_occ': new_patterns[['sup', 'pattern']].set_index('pattern').to_dict()['sup'],
            }


        return new_element
    
        
    def predict_new_characters(self, df: pd.DataFrame, agds: dict,
                               func, agds_func, 
                               pattern_func,
                               nosep_dir: str, pattern_dir: str,
                               word_occ_mode: str='raw', **kwargs):
        
        emo_words = kwargs.get('emo_words', [])
        
        results_proba = {k: dict(v) for d in df.apply(lambda row: 
                                                      
                              {row.author:  self.predict_new_character(self.create_new_element(row.text,
                                                                                               row.author,
                                                                                               nosep_dir=nosep_dir,
                                                                                               pattern_dir=pattern_dir,
                                                                                               word_occ_mode=word_occ_mode,
                                                                                               emo_words=emo_words),
                                                                       ch_ele,
                                                                       func,
                                                                       agds_func,
                                                                       pattern_func
                                                                      ).items()
                               
                              } , axis=1).tolist() for k, v in d.items()
        }
        
        
        results = {key: {k: max(v, key=v.get) for k, v in value.items()} for key, value in results_proba.items()}
        
        scores = {}
        res_df = pd.DataFrame.from_dict(results).T

        for cc in self.char_cols:
            scores[cc] = accuracy_score(df[cc], res_df[cc])
            
        return scores, results_proba, results

    def predict_new_character(self, new_element: dict, agds: dict, func, agds_func, pattern_func):
        
        ascs = {}
        
        n_char_patterns = {}
        for cc in self.char_cols:
            n_char_patterns[cc] = {key: sum(value['pattern_occ'].values()) for key, value in agds[cc].items()}
        n_all_patterns = sum(n_char_patterns[cc].values())
    
        
        for cc in char_cols:
            ascs.setdefault(cc, {})

            for cck, ccv in agds[cc].items():
                
                N_word = pdg.n_char_words[cc][cck] / pdg.n_all_words
                N_pattern = n_char_patterns[cc][cck] / n_all_patterns
                
                s1 = sum(
                    ( func(wocc) / agds_func(ccv['word_occ'].get(wid, 1)) ) / N_word \
                    
                    if ccv['word_occ'].get(wid, 0) > 0 else 0 \
                    
                    for wid, wocc in new_element['word_occ'].items()
                )
                
                s2 = sum(
                    ( pattern_func(func(pattern_occ), len(pattern)) / agds_func(ccv['pattern_occ'].get(pattern, 1)) ) / N_pattern \
                    
                    if ccv['pattern_occ'].get(pattern, 0) > 0 else 0 \
                    
                    for pattern, pattern_occ in new_element['pattern_occ'].items()
                )
                
                ascs[cc][cck] = s1 + s2
        
        return ascs