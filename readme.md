## Project Overview
This project is developed as part of a master thesis titled "Automatic classification and recognition of the character traits and needs of website users in order to intelligently predict their potential further actions and choices." The core aim is to create a machine learning model that precisely classifies personality traits using textual input, based on the Big Five personality model. The project utilizes natural language processing (NLP) techniques for preprocessing of texts and feature extraction, which are then used to train various machine learning models.

## Key Features
 - Utilization of a baseline dataset containing around 2500 essays labeled with Big Five personality traits.
 - Advanced data preprocessing techniques including **data cleaning, lemmatization and stop-word removal.**
 - Implementation of multiple NLP techniques for text preprocessing and feature extraction, including **Latent Semantic Analysis (LSA), Doc2Vec, and pattern mining algorithms.**
 - Integration of several machine learning models like LightGBM, SVM, Random Forest, and others for personality trait classification. 


## Models
The project investigated the performance of different machine learning models in predicting the Big Five personality traits from text data. Various feature extraction methods were applied to create datasets for model training and evaluation. The models tested include **KMeans, LDA, LightGBM, Logistic Regression, Naive Bayes, Random Forest, and SVM**. Each model was evaluated on four datasets differing in preprocessing techniques: **raw, lemma, no_stop_words, and no_stop_words_lemma.**

## Results

The results are primarily measured using the weighted F1 score for each personality trait (OPN, CON, EXT, AGR, NEU) across different datasets. Notably, the **LightGBM** model emerged as the standout performer, demonstrating exceptional proficiency in predicting personality traits. It achieved an impressive **average weighted F1 score of 89% across the five personality traits.**


<img src="https://gitlab.com/ttrzos/masters-project/-/raw/main/figures/radar_plot.png" align="center" height="600" width="600"/>

